using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BKKGraph.Graph
{
    public class DOTExporter
    {
        private class StringBuilderPool
        {
            //private List<StringBuilder> builders;
            private LinkedList<StringBuilder> freeBuilders;
            
            public StringBuilder GetBuilder()
            {
                StringBuilder res;
                lock (freeBuilders)
                {
                    if (freeBuilders.Count > 0)
                    {
                        res = freeBuilders.First.Value;
                        freeBuilders.RemoveFirst();
                    }
                    else
                    {
                        res = new StringBuilder();
                    }
                }

                return res;
            }

            public void ReleaseBuilder(StringBuilder sb)
            {
                lock (freeBuilders)
                {
                    freeBuilders.AddLast(sb);
                    sb.Clear();
                }
            }

            public StringBuilderPool(int initialBuilderNum)
            {
                freeBuilders = new LinkedList<StringBuilder>();
                for (int i = 0; i < initialBuilderNum; i++)
                {
                    freeBuilders.AddLast(new StringBuilder());
                }
            }
        }

        readonly int VertexExportGroups = 25;
        readonly int EdgeExportGroups = 50;

        private StringBuilderPool sbpool;

        public string Export(ICollection<Vertex> graph, bool usePosition = true)
        {
            StringBuilder sb = new StringBuilder("strict digraph {\n");
            var vertList = graph.ToList();
            ExportVertices(vertList, sb, usePosition).Wait();
            ExportEdges(vertList, sb).Wait();
            sb.Append("}");
            return sb.ToString();
        }
        
        //Each worker exports all edges of the provided vertices
        private void ExportVertexEdges(ICollection<Vertex> vertices, StringBuilder resSb)
        {
            StringBuilder tempSb = sbpool.GetBuilder();
            foreach (var vertex in vertices)
            {
                foreach (var edge in vertex.GetEdges())
                {
                    tempSb.Append($"{vertex.Data.Id} -> {edge.Target.Data.Id} [weight={edge.Weight}]\n");
                }
            }

            lock (resSb)
            {
                resSb.Append(tempSb);
            }
            sbpool.ReleaseBuilder(tempSb);
        }
        
        private async Task ExportEdges(List<Vertex> vertices, StringBuilder sb)
        {
            //var vertices = graph.ToList();
            var groupSize = vertices.Count / EdgeExportGroups;
            groupSize = (groupSize == 0) ? vertices.Count : groupSize;

            var tasks = new List<Task>(EdgeExportGroups + 1);
            for (int i = 0; i < vertices.Count; i += groupSize)
            {
                var taskSize = (i + groupSize < vertices.Count) ? groupSize : vertices.Count - i;
                //Ensure that the lambda doesn't try to access i after it is modified.
                var icapture = i;
                tasks.Add(Task.Run(() => ExportVertexEdges(vertices.GetRange(icapture, taskSize), sb)));
            }

            var all = Task.WhenAll(tasks);
            await all;
        }

        private void ExportVerticesTask(ICollection<Vertex> vertices, StringBuilder resSb, bool usePosition)
        {
            StringBuilder tempSb = sbpool.GetBuilder();
            foreach (var vertex in vertices)
            {
                var data = vertex.Data;
                tempSb.Append(data.Id).Append("[");
                if (data.Label != null)
                    tempSb.AppendLine($"label=\"{data.Label}\"");
                if (usePosition && data.Position != null)
                    tempSb.AppendLine($"pos=\"{data.Position.Value.x},{data.Position.Value.y}!\"");
                tempSb.AppendLine("]");
            }

            lock (resSb)
            {
                resSb.Append(tempSb);
            }
            sbpool.ReleaseBuilder(tempSb);
        }
        
        private async Task ExportVertices(List<Vertex> vertices, StringBuilder sb, bool usePosition)
        {
            var groupSize = vertices.Count / VertexExportGroups;
            groupSize = (groupSize == 0) ? vertices.Count : groupSize;

            var tasks = new List<Task>(VertexExportGroups + 1);
            for (int i = 0; i < vertices.Count; i += groupSize)
            {
                var taskSize = (i + groupSize < vertices.Count) ? groupSize : vertices.Count - i;
                //Ensure that the lambda doesn't try to access i after it is modified.
                var icapture = i;
                tasks.Add(Task.Run(() => ExportVerticesTask(vertices.GetRange(icapture, taskSize), sb, usePosition)));
            }

            var all = Task.WhenAll(tasks);
            await all;
        }
        
        public DOTExporter(int edgeExportGroups = 50, int vertexExportGroups = 25)
        {
            EdgeExportGroups = edgeExportGroups;
            VertexExportGroups = vertexExportGroups;
            sbpool = new StringBuilderPool(Math.Max(EdgeExportGroups, VertexExportGroups));
        }
    }
}