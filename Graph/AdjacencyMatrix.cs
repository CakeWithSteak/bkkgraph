using System;
using System.Collections.Generic;
using System.Linq;
using BKKGraph.GTFS;
using Edge = System.ValueTuple<string, string, BKKGraph.GTFS.RouteRecord.RouteType, float>;

namespace BKKGraph.Graph
{
    public class AdjacencyMatrix
    {
        private List<IVertexDataProvider> vertexData;
        private List<Edge> edges;

        public IEnumerable<Edge> Edges => edges;

        public IEnumerable<IVertexDataProvider> VertexData => vertexData;

        public AdjacencyMatrix()
        {
            vertexData = new List<IVertexDataProvider>();
            edges = new List<Edge>();
        }

        public AdjacencyMatrix(List<IVertexDataProvider> vertexData, List<Edge> edges)
        {
            this.vertexData = vertexData ?? throw new ArgumentNullException(nameof(vertexData));
            this.edges = edges ?? throw new ArgumentNullException(nameof(edges));
        }

        public void DeduplicateEdges()
        {
            edges = edges.Distinct().ToList();
        }

        public static AdjacencyMatrix FromGraph(IEnumerable<Vertex> vertices)
        {
            var res = new AdjacencyMatrix();
            foreach (var vertex in vertices)
            {
                res.vertexData.Add(vertex.Data);
                foreach (var edge in vertex.GetEdges())
                {
                    res.edges.Add((vertex.Data.Id, edge.Target.Data.Id, edge.RouteType, edge.Weight));
                }
            }
            res.DeduplicateEdges();

            return res;
        }
    }
}