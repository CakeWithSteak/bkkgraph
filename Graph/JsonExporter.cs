using System.Collections.Generic;
using System.Linq;
using Newtonsoft.Json;

namespace BKKGraph.Graph
{
    public class JsonExporter
    {
        //Quick hack for serializing tuples better
        private class AdjacencyMatrixSerializationWrapper
        {
            [JsonIgnore] private AdjacencyMatrix am;
            public IEnumerable<IVertexDataProvider> VertexData => am.VertexData;
            public IEnumerable<string[]> Edges => am.Edges.Select(e => new string[3] {e.Item1, e.Item2, e.Item3.ToString()});

            public AdjacencyMatrixSerializationWrapper(AdjacencyMatrix am)
            {
                this.am = am;
            }
        }

        public string Export(IEnumerable<Vertex> graph)
        {
            var am = AdjacencyMatrix.FromGraph(graph);
            var wrapper = new AdjacencyMatrixSerializationWrapper(am);
            string json = JsonConvert.SerializeObject(wrapper, Formatting.Indented);
            return json;
        }
    }
}