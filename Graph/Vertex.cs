using System;
using System.Collections.Generic;
using System.Linq;
using BKKGraph.GTFS;

namespace BKKGraph.Graph
{
    public class Vertex
    {
        public struct Edge
        {
            public Vertex Target;
            public RouteRecord.RouteType RouteType;
            public float Weight;

            public Edge(Vertex target, RouteRecord.RouteType routeType ,float weight = 1.0f)
            {
                Target = target;
                Weight = weight;
                RouteType = routeType;
            }
        }
        private ICollection<Edge> edges;

        public IVertexDataProvider Data;

        public void AddEdge(in Edge e)
        {
            edges.Add(e);
        }

        public void RemoveEdgesTo(Vertex target)
        {
            var edgesToRemove = edges.Where(e => e.Target == target).ToList();
            foreach (var edge in edgesToRemove)
            {
                edges.Remove(edge);
            }
        }

        public IEnumerable<Edge> GetEdges()
        {
            return edges;
        }

        public override int GetHashCode()
        {
            return Data.GetHashCode();
        }

        public Vertex(IVertexDataProvider data, ICollection<Edge> edges = null)
        {
            this.edges = edges ?? new LinkedList<Edge>();
            Data = data;
        }
    }
}