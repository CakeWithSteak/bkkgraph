using System;
using Newtonsoft.Json;

namespace BKKGraph.Graph
{
    //Provides data for formatting and exporting a graoh
    public interface IVertexDataProvider
    { 
        string Id { get; }
        //The x y coordinates of the vertex or null if unavailable
        (float x, float y)? Position { get; }
        string Label { get; }
    }
}