using System.Text;
using FileHelpers;

namespace BKKGraph.GTFS
{
    [DelimitedRecord(delimiter:",")]
    [IgnoreFirst]
    public class TripRecord
    {
        public string RouteId;
        
        public string Id;
        
        [FieldValueDiscarded] private string ServiceId;

        [FieldQuoted(QuoteMode.OptionalForBoth, MultilineMode.NotAllow)]
        public string Headsign;

        [FieldValueDiscarded] [FieldNullValue(0)] private int Direction;
        [FieldValueDiscarded] private string BlockId;
        [FieldValueDiscarded] private string ShapeId;
        [FieldValueDiscarded] [FieldNullValue(0)] private int WheelchairAccessible;
        [FieldValueDiscarded] [FieldNullValue(0)] private int BikesAllowed;
        [FieldValueDiscarded] [FieldNullValue(0)] private int BoardingDoor;
        
        public override string ToString()
        {
            var sb = new StringBuilder();
            foreach (var field in GetType().GetFields())
            {
                sb.Append($"{field.Name}: {field.GetValue(this)} ");
            }

            return sb.ToString();
        }
    }
}