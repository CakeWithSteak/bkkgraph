using System;
using System.Text;
using FileHelpers;

namespace BKKGraph.GTFS
{
    [DelimitedRecord(delimiter:",")]
    [IgnoreFirst]
    public class StopTimeRecord : IComparable<StopTimeRecord>, IEquatable<StopTimeRecord>
    {
        private class TimeConverter : ConverterBase
        {
            public override object StringToField(string from)
            {
                string[] fragments = from.Split(':');
                int hours = Int32.Parse(fragments[0]);
                int minutes = Int32.Parse(fragments[1]);
                int seconds = Int32.Parse(fragments[2]);
                int days = hours / 24;
                hours %= 24;

                return new TimeSpan(days, hours, minutes, seconds);
            }

            public override string FieldToString(object from)
            {
                var ts = (TimeSpan) from;
                return $"{ts.Hours + (ts.Days * 24):00}:{ts.Minutes:00}:{ts.Seconds:00}";
            }
        }
        
        public string TripId;
        public string StopId;

        [FieldConverter(typeof(TimeConverter))]
        public TimeSpan ArrivalTime;
        
        [FieldConverter(typeof(TimeConverter))]
        public TimeSpan DepartureTime;

        [FieldNotEmpty]
        public int StopSequence;

        [FieldQuoted(QuoteMode.OptionalForBoth, MultilineMode.NotAllow)] 
        [FieldOptional]
        public string StopHeadsign;

        [FieldOptional] [FieldValueDiscarded] private int? PickupType;
        [FieldOptional] [FieldValueDiscarded] private int? DropOffType;
        [FieldOptional] [FieldValueDiscarded] private float? ShapeDistanceTravelled;

        public int CompareTo(StopTimeRecord other)
        {
            return this.StopSequence - other.StopSequence;
        }

        public bool Equals(StopTimeRecord other)
        {
            if (other == null)
                return false;
            return StopId == other.StopId && TripId == other.TripId;
        }

        public override bool Equals(object obj)
        {
            return this.Equals(obj as StopTimeRecord);
        }

        public static bool operator >(StopTimeRecord a, StopTimeRecord b)
        {
            return a.CompareTo(b) > 0;
        }
        
        public static bool operator <(StopTimeRecord a, StopTimeRecord b)
        {
            return a.CompareTo(b) < 0;
        }

        public static bool operator ==(StopTimeRecord a, StopTimeRecord b)
        {
            if ((object)a == null || (object)b == null)
                return Object.Equals(a, b);
            return a.Equals(b);
        }
        
        public static bool operator !=(StopTimeRecord a, StopTimeRecord b)
        {
            return !(a == b);
        }
        
        public override string ToString()
        {
            var sb = new StringBuilder();
            foreach (var field in GetType().GetFields())
            {
                sb.Append($"{field.Name}: {field.GetValue(this)} ");
            }

            return sb.ToString();
        }
    }
}