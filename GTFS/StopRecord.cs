using System.Text;
using BKKGraph.Graph;
using FileHelpers;
using Newtonsoft.Json;

namespace BKKGraph.GTFS
{
    [DelimitedRecord(delimiter:",")]
    [IgnoreFirst]
    public class StopRecord : Graph.IVertexDataProvider
    {
        public enum Type
        {
            NoInfo = -1,
            Stop = 0,
            Station = 1,
            EntranceExit = 2
        }

        public enum WheelchairBoardingKind
        {
            NoInfo,
            Accessible,
            Inaccessible
        }

        public string Id;
        
        [FieldQuoted(QuoteMode.OptionalForBoth, MultilineMode.NotAllow)]
        public string Name;
        
        public float Latitude;
        public float Longitude;

        [FieldQuoted(QuoteMode.OptionalForBoth, MultilineMode.NotAllow)]
        [FieldOptional] [JsonIgnore] public string Code;

        [JsonIgnore]
        [FieldNullValue(Type.NoInfo)]
        [FieldOptional] 
        public Type LocationType;
        
        [JsonIgnore] [FieldOptional] public string ParentId;

        [JsonIgnore]
        [FieldNullValue(WheelchairBoardingKind.NoInfo)]
        [FieldOptional] public WheelchairBoardingKind WheelchairBoarding;

        [FieldOptional] [FieldValueDiscarded] private string Direction;

        public override string ToString()
        {
            var sb = new StringBuilder();
            foreach (var field in GetType().GetFields())
            {
                sb.Append($"{field.Name}: {field.GetValue(this)} ");
            }

            return sb.ToString();
        }

        public override int GetHashCode()
        {
            return Id.GetHashCode();
        }

        string IVertexDataProvider.Id => Id;

        [JsonIgnore]
        public (float x, float y)? Position => (Latitude, Longitude);

        [JsonIgnore]
        public string Label => Name;
    }
}