using FileHelpers;

namespace BKKGraph.GTFS
{
    [DelimitedRecord(",")]
    [IgnoreFirst]
    public class RouteRecord
    {
        public enum RouteType
        {
            Tram,
            Metro,
            Railway,
            Bus,
            Ferry,
            CableTram,
            AerialLift,
            Funicular
        }
        
        public string Agency;
        public string Id;
        public string ShortName;

        [FieldOptional] //[FieldQuoted(QuoteMode.OptionalForBoth, MultilineMode.NotAllow)]
        public string LongName;

        [FieldOptional] public RouteType Type;

        [FieldOptional] [FieldQuoted(QuoteMode.OptionalForBoth, MultilineMode.NotAllow)]
        public string Description;

        [FieldOptional] public string Color;
        [FieldOptional] public string TextColor;
    }
}