﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;
using FileHelpers;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using BKKGraph.Graph;
using BKKGraph.GTFS;

namespace BKKGraph
{
    class Program
    {
        private static void Main(string[] args)
        {
            //var prepped = PreprocessStopTimes(args[0], true);
            
            //args 0: stops
            //args 1: trips
            //args 2: routes
            //args 3: prepped stop times
            //args 4: json output
            //args 5: dot output

            Console.WriteLine("Reading files...");
            var data = ReadFiles(args[0], args[1], args[2], args[3]);
            
            Console.WriteLine("Creating graph...");
            var graph = CreateGraph(data.stopTimes, data.stops, data.trips, data.routes);
            //graph = graph.Where(v => v.GetEdges().Any()).ToList();
            
            
            Console.WriteLine("Exporting DOT...");
            var dotExporter = new DOTExporter();
            string dotRes = dotExporter.Export(graph);

            Console.WriteLine("Exporting json...");
            var jsonExporter = new JsonExporter();
            string jsonRes = jsonExporter.Export(graph);
            
            Console.WriteLine($"Result size: {(jsonRes.Length + dotRes.Length) / 1024.0 / 1024.0} MB\nSave? (y/n) ");

            var response = Console.Read();
            bool save = char.ToLower(Convert.ToChar(response)) == 'y';

            if (save)
            {
                Console.WriteLine("Saving...");
                using (var fs = File.CreateText(args[4]))
                {
                    fs.Write(jsonRes);
                }
                using (var fs = File.CreateText(args[5]))
                {
                    fs.Write(dotRes);
                }
            }
                
        }

        private static IList<StopTimeRecord> PreprocessStopTimes(string filepath, bool saveFile = false)
        {
            var engine = new DelimitedFileEngine<GTFS.StopTimeRecord>(Encoding.UTF8);
            var list = engine.ReadFile(filepath).ToList();
            list.Sort();

            if (saveFile)
            {
                string filename = Path.GetFileNameWithoutExtension(filepath);
                string savePath = filepath.Replace(filename, filename + "_prepped");
                engine.WriteFile(savePath, list);
            }
            
            return list;
        }

        private static ICollection<Vertex> CreateGraph(IEnumerable<StopTimeRecord> stopTimes,
            IDictionary<string, StopRecord> stops,
            IDictionary<string, TripRecord> trips,
            IDictionary<string, RouteRecord> routes)
        {
            var graph = new Dictionary<string, Vertex>();

            var visitedStops = new Dictionary<string, (string id, string tripId, TimeSpan time, int sequence)>();

            foreach (var stopTime in stopTimes)
            {
                string stopKey = trips[stopTime.TripId].RouteId + stopTime.TripId;

                if (!graph.TryGetValue(stopTime.StopId, out var thisVert))
                {
                    thisVert = new Vertex(stops[stopTime.StopId]);
                    graph.Add(stopTime.StopId, thisVert);
                }

                if (visitedStops.TryGetValue(stopKey, out var prevStop))
                {
                    /*if(prevStop.tripId == stopTime.TripId)
                        Debugger.Break();*/
                    
                    //Skip record on wrong seq number or trip id
                    if (prevStop.sequence >= stopTime.StopSequence || prevStop.tripId != stopTime.TripId) continue;

                    graph[prevStop.id]
                        .AddEdge(new Vertex.Edge(thisVert, routes[trips[stopTime.TripId].RouteId].Type ,W(stopTime.ArrivalTime - prevStop.time)));
                    visitedStops[stopKey] = (stopTime.StopId, stopTime.TripId, stopTime.DepartureTime,
                        stopTime.StopSequence);
                }
                else
                {
                    visitedStops[stopKey] = (stopTime.StopId, stopTime.TripId, stopTime.DepartureTime,
                        stopTime.StopSequence);
                }
            }

            return graph.Values;
        }
        
        //Weight function for edges
        private static float W(in TimeSpan t)
        {
            return (float)t.TotalMinutes;
        }

        private static (IDictionary<String, StopRecord> stops, 
            IDictionary<string, TripRecord> trips,
            IDictionary<string, RouteRecord> routes,
            IEnumerable<StopTimeRecord> stopTimes)
            ReadFiles(string stopsFilename, string tripsFilename, string routesFilename, string stopTimesFilename)
        {
            var stopsEngine = new FileHelperEngine<StopRecord>(Encoding.UTF8);
            var stops = stopsEngine.ReadFile(stopsFilename);
            
            var stopsDict = new Dictionary<string, StopRecord>(stops.Length);
            foreach (var stop in stops)
                stopsDict.Add(stop.Id, stop);
            
            var tripsEngine = new FileHelperEngine<TripRecord>(Encoding.UTF8);
            var trips = tripsEngine.ReadFile(tripsFilename);

            var tripsDict = new Dictionary<string, TripRecord>(trips.Length);
            foreach (var trip in trips)
                tripsDict.Add(trip.Id, trip);
            
            var routesEngine = new FileHelperEngine<RouteRecord>(Encoding.UTF8);
            var routes = routesEngine.ReadFile(routesFilename);

            var routesDict = new Dictionary<string, RouteRecord>(trips.Length);
            foreach (var route in routes)
                routesDict.Add(route.Id, route);
            
            var stopTimesEngine = new FileHelperEngine<StopTimeRecord>(Encoding.UTF8);
            var stopTimes = stopTimesEngine.ReadFile(stopTimesFilename);
           

            return (stopsDict, tripsDict, routesDict, stopTimes);
        }
    }
}